#!/bin/bash

mailaddress=rhein-main@lists.fsfe.org
#mailaddress=gaw@vergissmein.net
senderaddress=guido@fsfe.org


# getting the variables
if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1
# TODO check if source file was actually useful.


# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))
 
# the subject to be sent:
mailsubject="Die FSFE Fellowship-Gruppe Rhein/Main besucht $groupvisited in $city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y`"

if [ $daysleft \< 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the subject to be sent:
mailsubject="Nicht vergessen: Nicht mehr lange bis zum Fellowship-Treffen Rhein/Main in $city!"
fi



mailbody="Hallo allerseits,

die FSFE Fellowship-Gruppe Rhein/Main besucht am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` um $meetingtime Uhr $groupvisited im $location in $city. 

Alle an Freier Software Interessierte sind herzlich eingeladen!

Nähere Informationen finden sich wie immer auf der Wiki-Seite:
https://wiki.fsfe.org/LocalGroups/RheinMain

Vorschläge für die Agenda können auf dieser Wiki-Seite eingetragen werden:
https://wiki.fsfe.org/LocalGroups/RheinMain/Agenda

Viele Grüße

Guido


PS: Diese Nachricht wurde automatisch erstellt. Falls der Inhalt keinen Sinn
ergibt, oder sonstige Rückmeldungen bitte per Mail an mich. Danke!"


echo "$mailbody" | mail -a "Content-Type: text/plain; charset=UTF-8" -r "$senderaddress" -s "$mailsubject" "$mailaddress"

