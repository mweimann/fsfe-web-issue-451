#!/bin/bash

# this script shall add new meetings to the FSFE xml files
# usergroups.rheinmeinrocks.de test for validity and commit to github

# it shall check for all future events under fsferm/jobs, create xml snippets
# and assemble the fsfe.xml file. Add it to the ugrm repository, commit it and
# launch a merch request with the ugrm main repo



basedir="/home/guido/fsfe/fsferm/fsfe-web-issue-451/trunk"
groupname=fsferm

# real world repo
# localgit="/home/guido/programming/issue451/ugrm-data/"
# testing world repo
localgit="/home/guido/programming/testing"

# git file to be written
ugrmfile="$localgit/usergroup/fsfe.xml"



#before we do anything, we sync our repo with upstream
# according to https://help.github.com/articles/syncing-a-fork/

cd $localgit
echo "wir befinden uns jetzt in $localgit (oder sollten zumindest)"
#git fetch upstream
#git checkout master
#git merge upstream/master

# push the current state to remote (github)

echo "git push ..."
git push


# we should be good to go now as we are in sync with upstream and our own remote as well.

# create a new branch 


# This will always be the beginning of the ugrmfile file:

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<usergroup xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"../xsd/usergroup.xsd\">
    <name>Free Software Foundation Europe Fellowship Rhein-Main</name>
    <nickname>FSFE</nickname>
    <description>
     Treffen der Regional-Gruppe Rhein-Main der Free Software Foundation Europe
    </description>
    <url>https://wiki.fsfe.org/LocalGroups/RheinMain</url>
    <tags>
        <tag>Free Software</tag>
        <tag>Free Society</tag>
        <tag>Open Source</tag>
    </tags>
    <contact>
	<mailinglist>
            <url>https://lists.fsfe.org/mailman/listinfo/rhein-main</url>
		<label>Mailingliste</label>
	</mailinglist>
        <twitter>@FSFErm</twitter>
        <hashtag>#FSFErm</hashtag>
    </contact>
    <defaultmeetinglocation>
        <name>Café Albatros</name>
        <url>http://www.cafe-albatros.de/</url>
        <publictransport>U4, U6, U7, Tram 16 Bockenheimer Warte. S3, S4, S5 Frankfurt West</publictransport>
        <street>Kiesstrasse 27</street>
        <zip>60468</zip>
        <city>Frankfurt am Main</city>
        <region>Hessen</region>
        <country>Deutschland</country>
    </defaultmeetinglocation>
    <schedule usedefaultmeetinglocation=\"true\">
" > $ugrmfile


# Here we create the xml snippets for all future meetings under "jobs" and append them to ugrmfile



for i in $basedir/$groupname/jobs/* 
do 
if [ `echo $i |sed s/.*-//` -gt `date +%Y%m%d` ]
then
source $i
echo "        <meeting>
            <time>`date -d $dateofevent +%Y-%m-%d`T$meetingtime:00+02:00</time>
            <name>FSFE Fellowship Meeting</name>
            <url>https://wiki.fsfe.org/FellowshipEvents/Rhein-Main%20`date -d $dateofevent +%Y-%m-%d`</url>
            <location>
                <name>$location</name>
                <publictransport>$publictransport</publictransport>
                <street>$street</street>
                <zip>$zip</zip>
                <city>$city</city>
                <region>Hessen</region>
                <country>Deutschland</country>
            </location>
        </meeting>" >> $ugrmfile

fi
done

# Here we close the remaining tags and should have a file ready to commit
echo "    </schedule>
</usergroup>" >> $ugrmfile


# gitting serious

echo "wir betreten wieder $localgit"
cd $localgit
git checkout master

# create new branch for change

# TODO find newest file in jobs

file $ugrmfile
ugga=1
git commit -q $ugrmfile -m "added new event for $groupname" || ugga=2

if [ $ugga = 1 ]
then
echo "Let's go on"
else
echo "git commit didn't exit sucessful. Aborting..."
exit
fi


echo "I'll continue anyway!"
