#!/bin/bash


#diaspaccount=fsferm@despora.de

diaspaccount=fsferm@cryptospora.net

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1


diasptext="Das nächste #FSFE Fellowship-Treffen Rhein/Main findet am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` in #$city statt. $speaker wird einen Vortrag mit dem Titel \"$talktitle\" halten. Näheres unter: https://wiki.fsfe.org/LocalGroups/RheinMain #fsferm"


# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))

if [ $daysleft -lt 6 ]
   then 

# the message to be sent:
diasptext="Noch $daysleft Tage bis zum #FSFE Fellowship-Treffen Rhein/Main in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` https://wiki.fsfe.org/LocalGroups/RheinMain"
fi

if [ $daysleft -lt 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the message to be sent:
diasptext="Nicht vergessen! Nur noch $hoursleft Stunden bis zum #FSFE Fellowship-Treffen Rhein/Main in #$city https://wiki.fsfe.org/LocalGroups/RheinMain"
fi


echo "$diasptext"  | cliaspora -a $diaspaccount post public 
#echo "$diasptext" 

exit 0
